object CalculatorGUI: TCalculatorGUI
  Left = 0
  Top = 0
  Caption = 'CalculatorGUI'
  ClientHeight = 154
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object sbTestCalculator: TSpeedButton
    Left = 88
    Top = 51
    Width = 129
    Height = 45
    Caption = 'Show test calculator'
    OnClick = sbTestCalculatorClick
  end
end
