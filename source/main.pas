unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls;

type
  TCalculatorGUI = class(TForm)
    sbTestCalculator: TSpeedButton;
    procedure sbTestCalculatorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CalculatorGUI: TCalculatorGUI;

implementation

{$R *.dfm}

uses
  GUIMain;

procedure TCalculatorGUI.sbTestCalculatorClick(Sender: TObject);
var
  TestCalculatorGUI: TFCalculator;
begin

  TestCalculatorGUI := nil;
  try
    TestCalculatorGUI := TFCalculator.Create(Self);
    TestCalculatorGUI.ShowModal;
  finally
    FreeAndNil(TestCalculatorGUI);
  end;
end;

end.
