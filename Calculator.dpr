program Calculator;

uses
  Vcl.Forms,
  System.SysUtils,
  main in 'source\main.pas' {CalculatorGUI};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  FormatSettings.DecimalSeparator := '.';
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TCalculatorGUI, CalculatorGUI);
  Application.Run;
end.
